#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 24/Apr/17 20:19
Description:


"""
from os import walk
from sys import exit
from os.path import join
from csv import writer, reader
with open('data2.csv','a+' ) as f:
	walk_dir = 'data'
	for root, dirs, filenames in walk(walk_dir):
		for filename in filenames:
			w = writer(f)
			with open(join(walk_dir,filename)) as data:
				r = reader(data)
				next(r) # ignore the header of csv and continue
				for row in r:
					w.writerow(row)

print('Done')
