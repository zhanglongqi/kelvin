#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 7/Apr/17 18:43
Description:


"""
from json import loads, dump
from os import mkdir
from time import sleep

from common import *


def download_brief():
	try:
		mkdir('brief')
	except FileExistsError:
		pass

	page_base_url = 'https://doppler.finra.org/doppler-lookup/api/v1/search/individuals?city=NY&hl=true' \
									'&includePrevious=true&json.wrf=angular.callbacks._p&nrows=12&r=25&sort=&state=NY&wt=json&start='

	try:
		with open('last_page.json', 'r') as last_page_file:
			last_page = loads(last_page_file.read())['last_page']
	except FileNotFoundError:
		last_page = 0

	for page in range(last_page, 6924):  # page 6924
		print('Downloading page', page)
		page_url = page_base_url + str(page * 12)
		try:
			res = requests.get(page_url, headers=headers).content[25:-2]
		except (requests.exceptions.ConnectionError,
						ConnectionError) as e:
			with open('last_page.json', 'w') as last_page_file:
				dump({'last_page': page}, last_page_file)
			return False
		else:
			individuals_on_page = loads(res.decode('utf-8'))['results']['BROKER_CHECK_REP']['results']
			with open('brief/' + str(page) + ".json", "w") as page_file:
				dump(individuals_on_page, page_file)
			# sleep(5 * random())

	# if for loop finished
	return True


if __name__ == '__main__':

	# download brief individuals info for every page
	while True:
		if not download_brief():
			sleep(30)
			continue
		else:
			print('Downloading complete.')
			break
