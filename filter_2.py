#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 30/Apr/17 14:16
Description:


"""

from csv import reader, writer
from time import strptime, strftime

data = {}
with open('B.csv', ) as f:
	r = reader(f)
	next(r)
	for row in r:
		last_name = row[2]
		initial_first_name = row[1][:1]
		key = last_name + initial_first_name

		row[0] = int(row[0])  # convert id
		row[3] = strptime(row[3], '%m/%d/%Y')  # convert begin date
		row[4] = strptime(row[4], '%m/%d/%Y')  # convert end date
		if key in data:
			data[key].append(row)
		else:
			data[key] = [row, ]

with open('C.csv', 'w') as C:
	w = writer(C)
	w.writerow(
			['analyst', 'amaskcd', 'anndats',
			 'individual_id', 'first_name', 'last_name', 'begin', 'end', 'state', 'city', 'zipcode']
	)

	with open('A.csv', ) as f:
		r = reader(f)
		next(r)
		for row in r:
			name = row[0].split(' ')
			last_name = name[0]
			initial_first_name = name[len(name) - 1]
			key = last_name + initial_first_name

			if key in data:
				nominate = data[key]
				nominate2 = []
				working_date = strptime(row[2], '%m/%d/%Y')
				for n in nominate:
					if n[3] <= working_date <= n[4]:
						nominate2.append(n)

				for n in nominate2:
					target = list(n)
					target[3] = strftime('%m/%d/%Y', target[3])
					target[4] = strftime('%m/%d/%Y', target[4])
					new_row = row + target
					w.writerow(new_row)

				if len(nominate2) < 1:
					w.writerow(row)
			else:
				w.writerow(row)
