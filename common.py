#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 10/Apr/17 18:23
Description:


"""

import requests

headers = requests.utils.default_headers()
headers.update(
		{
			'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) '
										'Chrome/58.0.3029.54 Safari/537.36',
		}
)
