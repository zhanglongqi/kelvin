#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 30/Apr/17 14:16
Description:


"""

from csv import reader, writer

NY_state = set()
NY_city = set()
with open('2.csv', ) as f:
	r = reader(f)
	next(r)
	for row in r:
		if row[8] == 'NY':
			NY_state.add(row[3] + row[1][:1])
		if row[9] == 'NEW YORK':
			NY_city.add(row[3] + row[1][:1])

with open('3.csv', 'w') as f:
	w = writer(f)
	w.writerow(['analyst', 'amaskcd', 'NY state', 'New York city'])
	with open('1.csv', ) as f:
		r = reader(f)
		next(r)
		for row in r:
			name = row[0].split(' ')
			last_name = name[0]
			initial_first_name = name[len(name) - 1]

			if last_name + initial_first_name in NY_state:
				new_row = row + ['1']
			else:
				new_row = row + ['0']

			if last_name + initial_first_name in NY_city:
				new_row += ['1']
			else:
				new_row += ['0']

			w.writerow(new_row)
