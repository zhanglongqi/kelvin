#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 3/May/17 21:22
Description:


"""

from os import walk, stat, remove

from os.path import join

walk_dir = '/Users/longqi/Documents/kelvin/individual'
for root, dirs, filenames in walk(walk_dir):
	individuals = {}
	for filename in filenames:
		stat_info = stat(join(walk_dir, filename))
		if stat_info.st_size <= 2:
			print('removing', filename)
			remove(join(walk_dir, filename))
		# with open()
