#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 10/Apr/17 18:22
Description: 

"""

from json import loads, dump
from math import ceil
from os import mkdir
from os.path import join
from random import random
from time import sleep

from requests.exceptions import ConnectionError, ReadTimeout

from common import *


def download_brief(page_base_url, folder, page_number, selector, firm_id=0):
	try:
		mkdir(folder)
	except FileExistsError:
		pass
	last_pos_f = 'last_pos.json'
	try:
		with open(last_pos_f, 'r') as f:
			last_page = loads(f.read())[firm_id]
	except (FileNotFoundError, KeyError):
		last_page = 0

	for page in range(last_page, page_number):  # page number of firms in new york
		print('Downloading page', page)
		page_url = page_base_url + str(page * 12)
		try:
			res = requests.get(page_url, headers=headers, timeout=(10, 15)).content
		except (ConnectionError, ReadTimeout) as e:
			print('Error happen during downloading page', page, 'error!!!', e)
			print('Will retry in 30 seconds')

			with open(last_pos_f, 'w') as f:
				dump({firm_id: page}, f)
			return False
		else:
			res_json = loads(res.decode('utf-8'))
			for s in selector:
				res_json = res_json[s]
			with open(join(folder, str(firm_id) + '_' + str(page) + ".json"), "w") as f:
				dump(res_json, f)
			sleep(1 * random())

	# if for loop finished
	return True


if __name__ == '__main__':

	# download brief info of all firms in New York
	page_base_url = 'https://doppler.finra.org/doppler-lookup/api/v1/search/firms?city=NY&hl=true' \
									'&includePrevious=true&nrows=12&r=25&sort=&state=NY&wt=json&start='

	while False:
		if not download_brief(page_base_url=page_base_url, folder='firm', page_number=333,
													selector=('results', 'BROKER_CHECK_FIRM', 'results')):
			sleep(30)
			continue
		else:
			print('Downloading complete.')
			break

	for page in range(0, 333):
		print('Downloading brief of firm page', page)
		with open(join('firm', str(page) + '.json')) as page_f:
			for firm in loads(page_f.read()):
				firm_id = firm['fields']['bc_source_id']
				print('Downloading brief for firm', firm_id)
				page_url = 'https://doppler.finra.org/doppler-lookup/api/v1/search/individuals?city=NY&firm=' + firm_id + \
									 '&hl=true&includePrevious=true&nrows=12&r=25&sort=&state=NY&wt=json'

				page_nums = 0
				got_page_nums = False
				while not got_page_nums:
					try:
						res = requests.get(page_url, headers=headers, timeout=(10, 15)).content
					except (ConnectionError, ReadTimeout) as e:
						print('Error happen during getting page number for firm', firm_id, 'error!!!', e)
						print('Will retry in 30 seconds')
						sleep(30)
					else:
						page_nums = ceil(loads(res.decode('utf-8'))['results']['BROKER_CHECK_REP']['totalResults'] / 12)
						got_page_nums = True

				page_url = 'https://doppler.finra.org/doppler-lookup/api/v1/search/individuals?city=NY&firm=' + firm_id + \
									 '&hl=true&includePrevious=true&nrows=12&r=25&sort=&state=NY&wt=json&start='
				while True:
					if not download_brief(page_base_url=page_url, folder='brief', page_number=page_nums,
																selector=('results', 'BROKER_CHECK_REP', 'results'), firm_id=firm_id):
						sleep(10)
						continue
					else:
						print('Downloading of firm ' + firm_id + ' complete ')
						break
