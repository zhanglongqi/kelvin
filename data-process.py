#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 28/May/17 10:38
Description:
ID: 0~10609247

"""
import csv
from json import loads, dumps
from os import walk
from os.path import join
from uuid import uuid4 as uuid

data_dir = '/Users/longqi/Documents/kelvin/individual'
result_dir = '/Users/longqi/Documents/kelvin/'

test_scale = 1000


def setup_header():
	with open(join(result_dir, 'basic.csv'), 'w', newline='') as f:
		w = csv.writer(f)
		w.writerow((
			'individual id',
			'first name', 'middle name', 'last name',
		))

	with open(join(result_dir, 'work.csv'), 'w', newline='') as f:
		w = csv.writer(f)
		w.writerow((
			'individual id',
			'firm id', 'firm name',
			'begin', 'end',
			'state', 'city', 'zipcode',
		))

	with open(join(result_dir, 'status.csv'), 'w', newline='') as f:
		w = csv.writer(f)
		w.writerow((
			'individual id',
			'broker', 'ia', 'barred',
		))

	with open(join(result_dir, 'exam.csv'), 'w', newline='') as f:
		w = csv.writer(f)
		w.writerow((
			'individual id',
			'date', 'type', 'category', 'name',
		))

	with open(join(result_dir, 'disclosure.csv'), 'w', newline='') as f:
		w = csv.writer(f)
		w.writerow((
			'individual id',
			'event date', 'disclosure type', 'disclosure resolution',
			'Broker Comment', 'Judgment/Lien Type', 'criminalCharges_id', 'Initiated By', 'Judgment/Lien Amount',
			'arbitrationClaimFiledDetail', 'Damage Amount Requested', 'Firm Name', 'SanctionDetails_id', 'Settlement Amount',
			'Sanctions', 'Termination Type', 'DisplayAAOLinkIfExists', 'Disposition', 'Resolution', 'Allegations',
			'docketNumber', 'arbitrationDocketNumber', 'Damages Granted', 'Sanction Details', 'Type',
			'Description of Investigation'
		))

	with open(join(result_dir, 'criminalCharges.csv'), 'w', newline='') as f:
		w = csv.writer(f)
		w.writerow((
			'criminalCharges_id',
			'Charges', 'Charge Type', 'Disposition', 'Amended Charge Type', 'Amended Charge Disposition', 'Amended Charges'
		))

	with open(join(result_dir, 'SanctionDetails.csv'), 'w', newline='') as f:
		w = csv.writer(f)
		w.writerow((
			'SanctionDetails_id',
			'Sanctions', 'Amount', 'Duration', 'Start Date', 'End Date',
		))


def get_info():
	i = 0
	for root, dirs, filenames in walk(data_dir):
		for filename in filenames:
			with open(join(data_dir, filename)) as f:
				try:
					info = loads(f.read())
				except:
					print(filename)

				if len(info) == 0:
					continue
				ID = info['basicInformation']['individualId']
				if i >= test_scale:
					return None
				i += 1
				yield (ID, info)


def save_basic():
	with open(join(result_dir, 'basic.csv'), 'a', newline='') as f:
		w = csv.writer(f)
		for ID, info in get_info():
			print('save_basic', ID)

			first_name = info['basicInformation'].get('firstName', '')
			middle_name = info['basicInformation'].get('middleName', '')
			last_name = info['basicInformation'].get('lastName', '')

			w.writerow((
				ID, first_name, middle_name, last_name,
			))


def save_work():
	with open(join(result_dir, 'work.csv'), 'a', newline='') as f:
		w = csv.writer(f)

		for ID, info in get_info():
			print('save_work', ID)

			for firm in info.get('currentEmployments', []):
				for branchOffice in firm['branchOfficeLocations']:
					w.writerow((
						ID,

						firm['firmId'], firm['firmName'],
						firm.get('registrationBeginDate', ''), firm.get('registrationEndDate', ''),

						branchOffice.get('state', ''),
						branchOffice.get('city', ''),
						branchOffice.get('zipCode', '')
					))

			for firm in info.get('previousEmployments', []):
				w.writerow((
					ID,

					firm['firmId'], firm['firmName'],
					firm.get('registrationBeginDate', ''), firm.get('registrationEndDate', ''),

					firm.get('state', ''), firm.get('city', ''), firm.get('zipCode', '')
				))


def save_exam():
	with open(join(result_dir, 'exam.csv'), 'a', newline='') as f:
		w = csv.writer(f)

		for ID, info in get_info():
			print('save_exam', ID)

			for exam in info.get('stateExamCategory', []):
				exam_type = 'State Securities Law Exam'
				category = exam.get('examCategory', '')
				name = exam.get('examName', '')
				taken_date = exam.get('examTakenDate', '')
				w.writerow((ID, taken_date, exam_type, category, name,))

			for exam in info.get('principalExamCategory', []):
				exam_type = 'Principal/Supervisory Exam'
				category = exam.get('examCategory', '')
				name = exam.get('examName', '')
				taken_date = exam.get('examTakenDate', '')
				w.writerow((ID, taken_date, exam_type, category, name,))

			for exam in info.get('productExamCategory', []):
				exam_type = 'General Industry/Products Exam'
				category = exam.get('examCategory', '')
				name = exam.get('examName', '')
				taken_date = exam.get('examTakenDate', '')
				w.writerow((ID, taken_date, exam_type, category, name,))


def save_status():
	inactive = 'inactive'
	active = 'active'
	not_ia_bd = 'not'

	is_barred = 'yes'
	is_not_barred = 'no'

	status_index = {
		'inactive'         : inactive,
		'expanded'         : inactive,
		'legacy'           : inactive,
		'legacyarchived'   : inactive,
		'legacynonarchived': inactive,
		'active'           : active,
		'notinscope'       : not_ia_bd
	}

	with open(join(result_dir, 'status.csv'), 'a', newline='') as f:
		w = csv.writer(f)

		for ID, info in get_info():
			print('save_status', ID)

			broker = info['basicInformation'].get('bcScope', '').lower()
			ia = info['basicInformation'].get('iaScope', '').lower()

			broker_status = status_index.get(broker, broker)
			ia_status = status_index.get(ia, ia)

			barred_status = is_not_barred
			sanctions = info['basicInformation'].get('sanctions', None)
			if sanctions:
				barred = sanctions.get('permanentBar', '').lower()
				barred_status = is_barred if barred == 'y' or barred == 'yes' else is_not_barred

			w.writerow((ID, broker_status, ia_status, barred_status))


def save_disclosure():
	with open(join(result_dir, 'disclosure.csv'), 'a', newline='') as f:
		w = csv.writer(f)

		criminalCharges_w = csv.writer(open(join(result_dir, 'criminalCharges.csv'), 'a', newline=''))
		SanctionDetails_w = csv.writer(open(join(result_dir, 'SanctionDetails.csv'), 'a', newline=''))

		for ID, info in get_info():
			print('save_disclosure', ID)

			for disclosure in info.get('disclosures', []):
				event_date = disclosure.get('eventDate', '')
				disclosure_type = disclosure.get('disclosureType', '')
				disclosure_resolution = disclosure.get('disclosureResolution', '')
				disclosure_detail = disclosure.get('disclosureDetail', {})

				disclosure_detail = dumps(disclosure_detail)
				disclosure_detail = disclosure_detail.replace('\\r\\n', ' ')
				disclosure_detail = loads(disclosure_detail)

				Broker_Comment = disclosure_detail.get('Broker Comment', '')
				Judgment_Lien_Type = disclosure_detail.get('Judgment/Lien Type', '')

				# criminalCharges
				criminalCharges = disclosure_detail.get('criminalCharges', [])
				criminalCharges_id = uuid() if len(criminalCharges) > 0 else ''

				for criminalCharge in criminalCharges:
					Charges = criminalCharge.get('Charges', '')
					Charge_Type = criminalCharge.get('Charge Type', '')
					Disposition = criminalCharge.get('Disposition', '')
					Amended_Charge_Type = criminalCharge.get('Amended Charge Type', '')
					Amended_Charge_Disposition = criminalCharge.get('Amended Charge Disposition', '')
					Amended_Charges = criminalCharge.get('Amended Charges', '')
					criminalCharges_w.writerow((
						criminalCharges_id,
						Charges, Charge_Type, Disposition, Amended_Charge_Type, Amended_Charge_Disposition, Amended_Charges
					))

				Initiated_By = disclosure_detail.get('Initiated By', '')
				Judgment_Lien_Amount = disclosure_detail.get('Judgment/Lien Amount', '')
				arbitrationClaimFiledDetail = disclosure_detail.get('arbitrationClaimFiledDetail', '')
				Damage_Amount_Requested = disclosure_detail.get('Damage Amount Requested', '')
				Firm_Name = disclosure_detail.get('Firm Name', '')

				# to be used later
				SanctionDetails = disclosure_detail.get('SanctionDetails', [])
				SanctionDetails_id = uuid() if len(SanctionDetails) > 0 else ''

				for SanctionDetail in SanctionDetails:
					Sanctions = SanctionDetail.get('Sanctions', '')

					no_detail = True
					for detail in SanctionDetail.get('SanctionDetails', []):
						no_detail = False
						Amount = detail.get('Amount', '')
						Duration = detail.get('Duration', '')
						Start_Date = detail.get('Start Date', '')
						End_Date = detail.get('End Date', '')
						SanctionDetails_w.writerow((
							SanctionDetails_id,
							Sanctions, Amount, Duration, Start_Date, End_Date
						))

					if no_detail:
						SanctionDetails_w.writerow((
							SanctionDetails_id,
							Sanctions, '', '', '', ''
						))

				Settlement_Amount = disclosure_detail.get('Settlement Amount', '')
				Sanctions = disclosure_detail.get('Sanctions', '')
				Termination_Type = disclosure_detail.get('Termination Type', '')
				DisplayAAOLinkIfExists = disclosure_detail.get('DisplayAAOLinkIfExists', '')
				Disposition = disclosure_detail.get('Disposition', '')
				Resolution = disclosure_detail.get('Resolution', '')
				Allegations = disclosure_detail.get('Allegations', '')
				docketNumber = disclosure_detail.get('docketNumber', '')
				arbitrationDocketNumber = disclosure_detail.get('arbitrationDocketNumber', '')
				Damages_Granted = disclosure_detail.get('Damages Granted', '')
				Sanction_Details = disclosure_detail.get('Sanction Details', '')
				Type = disclosure_detail.get('Type', '')
				Description_of_Investigation = disclosure_detail.get('Description of Investigation', '')

				w.writerow((
					ID, event_date, disclosure_type, disclosure_resolution,
					Broker_Comment, Judgment_Lien_Type, criminalCharges_id, Initiated_By, Judgment_Lien_Amount,
					arbitrationClaimFiledDetail, Damage_Amount_Requested, Firm_Name, SanctionDetails_id, Settlement_Amount,
					Sanctions, Termination_Type, DisplayAAOLinkIfExists, Disposition, Resolution, Allegations,
					docketNumber, arbitrationDocketNumber, Damages_Granted, Sanction_Details, Type,
					Description_of_Investigation
				))


if __name__ == '__main__':
	print('setup header')
	setup_header()
	# save_basic()
	# save_work()
	# save_exam()
	# save_status()
	save_disclosure()
