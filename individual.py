# !/usr/bin/env python
# -*- coding: utf-8 -*-
"""
longqi 10/Apr/17 18:24
Description:


"""
from itertools import count
from json import loads, dump
from multiprocessing.pool import Pool
from os import walk
from os.path import join
from random import random
from time import sleep

from requests import ReadTimeout, ConnectionError

from common import *

data_dir = '../individual'


def make_individual_url(individualId):
	return 'https://doppler.finra.org/doppler-lookup/api/v1/search/individuals/' + individualId + '/?&wt=json'


def get_individual(individual_id):
	print(individual_id, end='\t')

	individual_info = None

	is_got = False
	while not is_got:
		try:
			res = requests.get(make_individual_url(individual_id),
			                   headers=headers, timeout=(5, 10)).content
		except (ConnectionError, ReadTimeout) as e:
			print('Getting individual', individual_id, 'error!!!', e)
			print('Will continue in a few seconds')
			is_got = False
			sleep(5 * random())
		else:
			is_got = True
			res = res.decode()
			individualNum = loads(res)['results']['BROKER_CHECK_REP']['totalResults']
			if individualNum > 0:
				individual_info = loads(loads(res)['results']['BROKER_CHECK_REP']['results'][0]['fields']
				                        ['content_json'][0])
			else:
				individual_info = None

	with open(join(data_dir, individual_id + '.json'), 'w') as f:
		if individual_info:
			dump(individual_info, f)
		else:
			f.write('{}')
	return individual_info


if __name__ == '__main__':

	t_num = 4

	downloaded = set()
	for root, dirs, filenames in walk(data_dir):
		for filename in filenames:
			downloaded.add(filename[:-5])

	for i in count():
		individualIds0 = (str(ID) for ID in range(i * t_num, (i + 1) * t_num))
		individualIds1 = []
		for ID in individualIds0:
			if ID not in downloaded:
				individualIds1.append(ID)

		# for ID in individualIds1:
		# 	get_individual(ID)
		if len(individualIds1) <= 0:
			continue

		with Pool(len(individualIds1)) as pool:
			pool.map(get_individual, individualIds1)
